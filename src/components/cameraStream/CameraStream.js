import React, {useEffect, useMemo} from 'react';

const CameraStream = (
  {
    videoRef,
    isLoading,
    width,
    height
  }
) => {
  useEffect(() => {
    let constraints = {audio: false, video: {width, height}};
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((mediaStream) => {
        let video = videoRef.current;
        video.srcObject = mediaStream;
        video.onloadedmetadata = function (e) {
          video.play();
        };
      })
      .catch(function (err) {
        console.log(err.navigator + ": " + err.message);
      });
  }, [videoRef, width, height]);

  const renderAnimation = useMemo(() => {
    if (!isLoading) {
      return null;
    }
    return (
      <div className={"loading-wrapper"}>
        <p>Loading models...</p>
        <div className="lds-roller">
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
        </div>
      </div>
    );
  }, [isLoading])

  return (
    <div>
      {renderAnimation}
      <div id="container">
        <video autoPlay={true} id="VideoElement" ref={videoRef} controls/>
      </div>
    </div>
  );
};

export default CameraStream;
