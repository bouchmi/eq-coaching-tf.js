import {useCallback, useEffect, useState} from 'react';

const OSWALD_EVENT_NAMES = {
  WIDGET_READY: 'oswald-widget-ready',
  USER_MESSAGE: 'oswald-user-message',
  MESSAGE: 'oswald-message',
  CLOSE_WIDGET: 'oswald-close-iframe',
  OPEN_WIDGET: 'oswald-open-iframe',
  DATA: 'oswald-data',
};

export default function useOswald() {
  const widgetId = 'oswald-widget';
  const bubbleId = 'oswald-widget-bubble';

  const [latestMessageTimestamp, setLatestMessageTimestamp] = useState(new Date().getTime());
  const [latestUserMessageTimestamp, setLatestUserMessageTimestamp] = useState(new Date().getTime());
  const [widgetIsReady, setWidgetIsReady] = useState(false);
  const [widgetIsOpen, setWidgetIsOpen] = useState(false);

  const handleEventActions = useCallback((eventName, event) => {
    switch (eventName) {
      case OSWALD_EVENT_NAMES.WIDGET_READY:
        setWidgetIsReady(true);
        break;
      case OSWALD_EVENT_NAMES.CLOSE_WIDGET:
        setWidgetIsOpen(false);
        break;
      case OSWALD_EVENT_NAMES.OPEN_WIDGET:
        setWidgetIsOpen(true);
        break;
      case OSWALD_EVENT_NAMES.MESSAGE: {
        // const data = event.data?.data;
        const timestamp = new Date().getTime();
        setLatestMessageTimestamp(timestamp);
        break;
      }
      case OSWALD_EVENT_NAMES.USER_MESSAGE: {
        // const data = event.data?.data;
        const timestamp = new Date().getTime();
        setLatestUserMessageTimestamp(timestamp);
        break;
      }
      default:
        break;
    }
  }, [])

  const sendDataToOswald = useCallback((metadata = {}) => {
    if (!widgetIsReady) {
      return 'Oswald widget not ready yet.'
    }
    let dataToSend = {eventName: OSWALD_EVENT_NAMES.DATA, metadata: metadata};
    document.getElementById(widgetId).contentWindow.postMessage(dataToSend, '*');
    return 'Metadata has been sent.'
  }, [widgetIsReady]);

  useEffect(() => {
    window.addEventListener('message', (event) => {
      const eventName = event.data?.eventName;
      handleEventActions(eventName, event);
    })
    return () => window.removeEventListener('message', () => {
    });
  }, [handleEventActions]);

  return [
    sendDataToOswald,
    latestMessageTimestamp,
    latestUserMessageTimestamp,
    widgetIsOpen,
    widgetId,
    bubbleId,
  ];
}
