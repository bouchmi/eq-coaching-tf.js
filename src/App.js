import React, {useCallback, useEffect, useRef, useState} from 'react';
import './App.scss';
import CameraStream from "./components/cameraStream/CameraStream";
import useOswald from "./hooks/useOswald";
import * as faceapi from "face-api.js";
 import * as tf from '@tensorflow/tfjs';

faceapi.env.monkeyPatch({
  Canvas: HTMLCanvasElement,
  Image: HTMLImageElement,
  ImageData: ImageData,
  Video: HTMLVideoElement,
  createCanvasElement: () => document.createElement('canvas'),
  createImageElement: () => document.createElement('img')
})
const App = () => {
  const videoRef = useRef(null);
  const [isLoading, setIsLoading] = useState(true);
  const [sendDataToOswald, latestMessageTimestamp] = useOswald();
  const width = 1200;
  const height = 900;
  const [recognizedVariables, setRecognizedVariables] = useState([]);
  const [emotion, setEmotion] = useState('neutral');

  const currentVariables = useCallback(() => {
    let previous = [0, 0, 0, 0, 0, 0, 0];
    const filteredResults = recognizedVariables.filter((item) => (item.timestamp - latestMessageTimestamp) > 0);
    filteredResults.forEach((variable) => {
      const diff = variable.timestamp - latestMessageTimestamp;
      const movingAverage = tf.movingAverage(previous, Object.values(variable).slice(0, 7), 0.95, diff, false);
      previous = movingAverage.arraySync();
    });
    return previous;
  }, [latestMessageTimestamp, recognizedVariables]);

  useEffect(() => {
    const result = currentVariables();
    const movingAverage = result.slice(0,7);
    const options = {
      0: 'neutral',
      1: 'happy',
      2: 'sad',
      3: 'angry',
      4: 'fearful',
      5: 'disgusted',
      6: 'surprised',
      7: 'timestamp',
    }
    const index = movingAverage.indexOf(Math.max(...movingAverage));

    if (options[index] !== emotion) {
      setEmotion(options[index]);
    }
  }, [currentVariables, emotion])

  useEffect(() => {
    console.log("Current emotion", emotion);
    console.log(sendDataToOswald({emotion}));
  }, [sendDataToOswald, emotion])

  useEffect(() => {
    faceapi.loadSsdMobilenetv1Model('/models').then((mobileNet) => {
      console.log(mobileNet)
      faceapi.loadFaceExpressionModel('/models').then((loadFaceExpression) => {
        setIsLoading(false);
      })
    })
  }, [])

  useEffect(() => {
      if (isLoading && videoRef && videoRef.current) {
        return;
      }
      videoRef.current.addEventListener("playing", () => {
        let createdCanvas = faceapi.createCanvasFromMedia(videoRef.current);
        document.body.append(createdCanvas)
        const displaySize = {width, height};
        faceapi.matchDimensions(createdCanvas, displaySize);
        // set interval is async fn
        // the handler runs every 100 ms
        setInterval(async () => {
          const detections = await faceapi
            .detectAllFaces(videoRef.current)
            // .withFaceLandmarks()
            .withFaceExpressions();
          const timestamp = new Date().getTime();
          const faceExpression = (detections[0] || {}).expressions;
          if (faceExpression) {
            const combinedFaceExpression = {...faceExpression, timestamp}
            setRecognizedVariables(r => [...r, combinedFaceExpression]);
          }
          // const resizedDetections = faceapi.resizeResults(detections, displaySize);
          // clear canvas before drawing new detections
           setIsLoading(false);
          // createdCanvas.getContext("2d").clearRect(0, 0, createdCanvas.width, createdCanvas.height)
          // faceapi.draw.drawDetections(createdCanvas, resizedDetections);
          // faceapi.draw.drawFaceLandmarks(createdCanvas, resizedDetections);
          // faceapi.draw.drawFaceExpressions(createdCanvas, resizedDetections);
        }, 200);
      })
    // }).catch((error) => {
    //   console.error(error);
    //   return error;
    // });
  }, [sendDataToOswald, isLoading]);

  return (
    <div>
      <span className="emotion-show">{emotion}</span>
      <CameraStream width={width} height={height} isLoading={isLoading} videoRef={videoRef}/>
    </div>
  );
};

export default App;
